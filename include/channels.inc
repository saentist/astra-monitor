<script> 
$(document).ready(function() { 
    $('#select_channel_all').click(function(event) {  //on click
        if(this.checked) { // check select status
            $((':input[name="select_channel[]"]')).each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $((':input[name="select_channel[]"]')).each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
	$("#channel_sort").tablesorter({ 
        headers: { 
            0: { sorter: false }, 
            4: { sorter: false }, 
            5: { sorter: false } 
        } 
    }); 
}); 
</script>

<div class="filter">
	<form action="index.php?page=channels" method="post" id="filter"></form>
	<form action="index.php?page=channels&delete=channels" method="post" id="delete_channel"></form>
		<table>
		<tr>
			<td>Сервер</td>
			<td>
				<select name="filter[where][channels.server]" form="filter">
				<option value=''>Все</option>';
				<?php 
				$GetChannelServer=$Astra->get_server();
				if (is_array($GetChannelServer)) {
					foreach ($GetChannelServer as $Server){
						echo '<option value='.$Server["server"].' '.((isset($_POST["filter"]["where"]["channels.server"]) && $Server["server"]==$_POST["filter"]["where"]["channels.server"])? 'selected':'').'>'.$Server["server"].'</option>';
					}
				}
				?>
				</select>
			</td>
			<td>Поток</td>
			<td>
				<select name="filter[where][channels.stream]" form="filter">
				<option value=''>Все</option>';
				<?php 
				$GetChannelStream=$Astra->get_channel_stream();
				if (is_array($GetChannelStream)) {
					foreach ($GetChannelStream as $Stream){
						echo '<option value='.$Stream["stream"].' '.((isset($_POST["filter"]["where"]["channels.stream"]) && $Stream["stream"]==$_POST["filter"]["where"]["channels.stream"])? 'selected':'').'>'.$Stream["stream"].'</option>';
					}
				}
				?>
				</select>
			</td>
			<td>Состояние</td>
			<td>
				<select name="filter[where][channels.ready]" form="filter">
				<option value=''>Все</option>';
				<option value='1' <?=((isset($_POST["filter"]["where"]["channels.ready"]) && $_POST["filter"]["where"]["channels.ready"] == '1')? 'selected':'')?>>Работает</option>';
				<option value='0' <?=((isset($_POST["filter"]["where"]["channels.ready"]) && $_POST["filter"]["where"]["channels.ready"] == '0')? 'selected':'')?>>Не работает</option>';
				</select>
			</td>
			<td>
				<input type="submit" value="Показать" id="sumbit" form="filter">
			</td>
			<td>
				<input type="submit" value="Удалить выбранные" id="sumbit" form="delete_channel">
			</td>
		</tr>
	
		</table>
		
	
</div>

	
<table id="channel_sort" class="tablesorter">
	<thead> 
	    <tr>
		<th><input type='checkbox' id='select_channel_all'></th><th>#</th><th>Канал</th><th>Output</th><th>Работает</th><th>Открыт</th><th>Битрейт</th><th>CC</th><th>PES</th><th>Поток</th><th>Сервер</th><th>Обновлен</th>
	    </tr>
	</thead> 
	<tbody>
		<?php
		$where = isset($_REQUEST["filter"]["where"]) ? $_REQUEST["filter"]["where"] : '';
		if ($GetChannel=$Astra->get_channel($where)) {
			foreach ($GetChannel as $ChannelKey => $Channel){
				//print_r($GetChannel);
				echo "<tr class='find_string'>
					<td><input type='checkbox' name='select_channel[]' value='{$Channel["id"]}' form='delete_channel'></td>
					<td>".($ChannelKey+1)."</td>
					<td id='left_bold'>{$Channel["channel"]}</td>
					<td id='left_bold'><a title='Открыть лог' href='?page=logs&filter[where][log.output]={$Channel["output"]}'>{$Channel["output"]}</a></td>
					<td ".(($Channel["ready"]=='0')? 'style="background: rgb(255, 236, 97);"':'')."><img src='img/icons/".(($Channel["ready"]=='1')? 'connect.png':'disconnect.png')."'></td>
					".(($Channel["scrambled"]=='1')? '<td style="background: rgb(255, 149, 60);"><img src="img/icons/lock.png"></td>':'<td><img src="img/icons/lock_unlock.png"></td>')."
					<td ".(($Channel["bitrate"] <100 )? 'style="background: rgb(255, 218, 218);"':'').">{$Channel["bitrate"]}</td>
					
					<td>{$Channel["cc_error"]}</td>
					<td>{$Channel["pes_error"]}</td>
					<td id='left_bold'><a title='Открыть лог' href='?page=logs&filter[where][log.stream]={$Channel["stream"]}'>{$Channel["stream"]}</a></td>
					<td id='left_bold'><a title='Открыть лог' href='?page=logs&filter[where][log.server]={$Channel["server"]}'>{$Channel["server"]}</a></td>
					<td ".(($Channel["last_update_period"] > 10 * 60 )? 'style="background: rgb(255, 100, 100);"':'').">{$Channel["last_update"]}</td>
				</tr>";
			}
		}
		
		?>
	</tbody>
</table>