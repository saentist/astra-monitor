<script> 
$(document).ready(function() { 
    $('#select_adapter_all').click(function(event) {  //on click
        if(this.checked) { // check select status
            $((':input[name="select_adapter[]"]')).each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"               
            });
        }else{
            $((':input[name="select_adapter[]"]')).each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });         
        }
    });
	$("#adapter_sort").tablesorter({ 
        headers: { 
            0: { sorter: false }, 
            5: { sorter: false }, 
            6: { sorter: false }, 
            7: { sorter: false }  
        } 
    }); 
}); 
</script>

<div class="filter">
	<form action="index.php?page=adapters" method="post" id="filter"></form>
	<form action="index.php?page=adapters&delete=adapters" method="post" id="delete_adapter"></form>
		<table>
		<tr>
			<td>Сервер</td>
			<td>
				<select name="filter[where][channels.server]" form="filter">
				<option value=''>Все</option>';
				<?php 
				$GetChannelServer=$Astra->get_server();
				if (is_array($GetChannelServer)) {
					foreach ($GetChannelServer as $Server){
						echo '<option value='.$Server["server"].' '.((isset($_REQUEST["filter"]["where"]["channels.server"]) && $Server["server"]==$_REQUEST["filter"]["where"]["channels.server"])? 'selected':'').'>'.$Server["server"].'</option>';
					}
				}
				?>
				</select>
			</td>
			<td>
				<input type="submit" value="Показать" id="sumbit" form="filter">
			</td>
			<td>
				<input type="submit" value="Удалить выбранные" id="sumbit" form="delete_adapter">
			</td>
		</tr>
		</table>

</div>

	
<table id="adapter_sort" class="tablesorter">
	<thead>
		<tr>
			<th><input type='checkbox' id='select_adapter_all'></th><th>#</th><th>Сервер</th><th>Поток</th><th>Адаптер</th><th>Сигнал</th><th>Уровнь сигнала</th><th>Качество</th><th>Битрейт/кбит</th><th title="Коэффициент битовых ошибок">ber</th><th title="Блоки с ошибками">unc</th><th>Обновлен</th>
		</tr>		
	</thead>
	<tbody>
		<?php
		$where = isset($_REQUEST["filter"]["where"]) ? $_REQUEST["filter"]["where"] : '';
		if ($GetAdapter=$Astra->get_adapter($where)) {
			foreach ($GetAdapter as $AdapterKey => $Adapter){
				//print_r($GetChannel);
				echo "<tr class='find_string'>
					<td><input type='checkbox' name='select_adapter[]' value='{$Adapter["id"]}' form='delete_adapter'></td>
					<td>".($AdapterKey+1)."</td>
					<td id='left_bold'>{$Adapter["server"]}</td>
					<td id='left_bold'><a title='Открыть лог' href='?page=logs&filter[where][log.stream]={$Adapter["stream"]}'>{$Adapter["stream"]}</a></td>
					<td>{$Adapter["adapter"]}</td>
					<td ".(($Adapter["lock"]  == 31 )? '':'style="background: rgb(255, 218, 218);"')."><img src='img/icons/antenna.png'></td>
					<td><meter min='0' low='40' high='80' max='100' optimum='100' value='{$Adapter["signal"]}'>{$Adapter["signal"]}</meter> {$Adapter["signal"]}</td>
					<td><meter min='0' low='40' high='80' max='100' optimum='100' value='{$Adapter["snr"]}'>{$Adapter["snr"]}</meter> {$Adapter["snr"]}</td>
					<td ".(($Adapter["bitrate_sum"] < 200 )? 'style="background: rgb(255, 218, 218);"':'').">{$Adapter["bitrate_sum"]}</td>
					<td>{$Adapter["ber"]}</td>
					<td>{$Adapter["unc"]}</td>
					<td ".(($Adapter["last_update_period"] > 10 * 60 )? 'style="background: rgb(255, 100, 100);"':'').">{$Adapter["last_update"]}</td>
				</tr>";
			}
		}
		
		?>
	</tbody>
</table>