--
-- ��������� ������� `adapters`
--

CREATE TABLE IF NOT EXISTS `adapters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adapter_id` int(11) unsigned NOT NULL,
  `adapter` int(11) unsigned NOT NULL DEFAULT '0',
  `server` varchar(100) NOT NULL DEFAULT '',
  `stream` varchar(100) NOT NULL DEFAULT '',
  `lock` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `signal` int(11) unsigned DEFAULT '0',
  `snr` int(11) unsigned NOT NULL DEFAULT '0',
  `bitrate` int(11) unsigned NOT NULL DEFAULT '0',
  `unc` int(11) unsigned NOT NULL DEFAULT '0',
  `ber` int(11) unsigned NOT NULL DEFAULT '0',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `adapter_id` (`adapter_id`,`server`),
  KEY `server` (`server`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=251568 ;

-- --------------------------------------------------------

--
-- ��������� ������� `channels`
--

CREATE TABLE IF NOT EXISTS `channels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) unsigned NOT NULL,
  `server` varchar(100) NOT NULL DEFAULT '' COMMENT '��� �����',
  `output` varchar(100) NOT NULL DEFAULT '' COMMENT '�����, ������������ ������ ������� ������ output',
  `channel` varchar(100) NOT NULL DEFAULT '' COMMENT '�������� ������',
  `stream` varchar(100) NOT NULL DEFAULT '',
  `ready` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '��������������� � true ���� �������� ��� ����������� ���������� �� ������',
  `cc_error` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '������� CC-������',
  `pes_error` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '������� ������ PES',
  `bitrate` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '�������� ������',
  `scrambled` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '���������� ��� ��� �����',
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `channel_id` (`channel_id`,`server`),
  KEY `server` (`server`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=782095 ;

--
-- �������� `channels`
--
DROP TRIGGER IF EXISTS `channel_log_trigger`;
DELIMITER //
CREATE TRIGGER `channel_log_trigger` BEFORE UPDATE ON `channels`
 FOR EACH ROW begin
    if new.ready != old.ready or new.scrambled != old.scrambled then
      insert delayed into `log` set server = new.server,
 			 `stream` = new.stream,   
             `channel` = new.channel,             
             `output` = new.output,             
             `ready` = new.ready,
             `scrambled` = new.scrambled;
    end if;
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- ��������� ������� `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `log_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `server` varchar(100) NOT NULL DEFAULT '' COMMENT '��� �����',
  `stream` varchar(100) NOT NULL DEFAULT '',
  `output` varchar(100) NOT NULL DEFAULT '' COMMENT '�����, ������������ ������ ������� ������ output',
  `channel` varchar(100) NOT NULL DEFAULT '',
  `ready` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `scrambled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`log_id`),
  KEY `stream` (`stream`),
  KEY `server` (`server`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3212 ;

-- --------------------------------------------------------

--
-- ��������� ������� `server`
--

CREATE TABLE IF NOT EXISTS `server` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `server` (`server`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- ��������� ������� `server_conf`
--

CREATE TABLE IF NOT EXISTS `server_conf` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(100) NOT NULL,
  `conf` varchar(100) NOT NULL,
  `res` int(1) DEFAULT NULL,
  `tune` int(11) DEFAULT NULL,
  `channel` int(11) DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `server` (`server`,`conf`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=762805 ;

--
-- ����������� �������� ����� ����������� ������
--

--
-- ����������� �������� ����� ������� `adapters`
--
ALTER TABLE `adapters`
  ADD CONSTRAINT `adapters_ibfk_1` FOREIGN KEY (`server`) REFERENCES `server` (`server`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- ����������� �������� ����� ������� `channels`
--
ALTER TABLE `channels`
  ADD CONSTRAINT `channels_ibfk_1` FOREIGN KEY (`server`) REFERENCES `server` (`server`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- ����������� �������� ����� ������� `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`server`) REFERENCES `server` (`server`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- ����������� �������� ����� ������� `server_conf`
--
ALTER TABLE `server_conf`
  ADD CONSTRAINT `server_conf_ibfk_1` FOREIGN KEY (`server`) REFERENCES `server` (`server`) ON DELETE CASCADE ON UPDATE CASCADE;
